/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/


function callback1(boardsData, id, callbackFunction) {
    setTimeout(() => {
        if (Array.isArray(boardsData) &&
            typeof (id) === 'string' &&
            typeof (callbackFunction) === 'function') {

            for (let index in boardsData) {
                if (boardsData[index].id === id) {
                    callbackFunction(null, boardsData[index]);
                }
            }

        }

    }, 2 * 1000);
}

module.exports = callback1;