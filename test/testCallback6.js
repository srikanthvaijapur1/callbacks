//Data files
const boardsData = require("../data/boards.json");
const listsData = require("../data/lists.json");
const cardsData = require("../data/cards.json");

//module
const callback6 = require("../callback6");


let testData = {
    boardsName: 'Thanos'
};

callback6(boardsData,listsData,cardsData,testData);