//Data files
const boardsData = require("../data/boards.json");
const listsData = require("../data/lists.json");
const cardsData = require("../data/cards.json");

//module
const callback4 = require("../callback4");


let testData = {
    boardsName: 'Thanos',
    cardName: 'Mind'
}

callback4(boardsData, listsData, cardsData, testData);