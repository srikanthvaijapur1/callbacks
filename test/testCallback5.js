//Data files
const boardsData = require("../data/boards.json");
const listsData = require("../data/lists.json");
const cardsData = require("../data/cards.json");

//module
const callback5 = require("../callback5");


let testData = {
    boardsName: 'Thanos',
    mindCard: 'Mind',
    spaceCard:'Space'
};

callback5(boardsData,listsData,cardsData,testData);