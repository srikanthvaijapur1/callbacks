/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

function callback2(listsData, id, callbackFunction) {
    setTimeout(() => {
        if (typeof (listsData) === 'object' &&
            typeof (id) === 'string' &&
            typeof (callbackFunction === 'function')) {
            if (listsData[id]) {
                callbackFunction(null, listsData[id])
            } else {
                callbackFunction({ message: 'List id not found' });
            }
        }

    }, 2 * 1000)

}





module.exports = callback2;