/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

function callback3(cardsData, id, callbackFunction) {
    setTimeout(() => {
        if (typeof (cardsData) === 'object' &&
            typeof (id) === 'string' &&
            typeof (callbackFunction === 'function')) {
            if (cardsData[id]) {
                callbackFunction(null, cardsData[id])
            } else {
                console.log("Cards not found");
            }

        }
    }, 2 * 1000)
};


module.exports = callback3;