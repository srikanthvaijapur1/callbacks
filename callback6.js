/* 
    Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const callback2 = require("./callback2");
const callback3 = require("./callback3");


function callback6(boardsData, listsData, cardsData, testData) {
    setTimeout(() => {

        let thanosBoardInfo;
        let thanosListInfo;
        let mindCardsInfo = [];

        for (let index in boardsData) {
            if (testData.boardsName === boardsData[index].name) {
                thanosBoardInfo = boardsData[index];
            }
        }
        console.log('thanosBoardInfo', thanosBoardInfo);

        let id = thanosBoardInfo.id;

        callback2(listsData, id, getThanosList = (error, data) => {
            if (error) {
                console.log(error);
            } else {
                thanosListInfo = data;
                console.log('thanosListInfo', thanosListInfo);

                for (let index in thanosListInfo) {
                    let cardId = thanosListInfo[index].id
                    let cardName = thanosListInfo[index].name

                    callback3(cardsData,cardId,getCardInfo = (error,data)=>{
                        if(error){
                            console.log(error);
                        }else{
                            mindCardsInfo.push(cardName,data)
                            console.log(mindCardsInfo)
                        }
                    })

                }


            }

        })



    }, 2 * 1000)
    return;
}




module.exports = callback6;